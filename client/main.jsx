import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { renderRoutes } from '../imports/startup/client/routes.js';

//Importing Boostrap
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

//Importing FontAwesome
import '@fortawesome/fontawesome-free/js/all.js';

import 'animate.css/animate.css'

Meteor.startup(() => {
  render(renderRoutes(), document.getElementById('react-target'));
});
