import React from 'react';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';

// route pages
import Index from '../../ui/Pages/Index.jsx';
import Gallery from '../../ui/Pages/Gallery.jsx';

const browserHistory = createBrowserHistory();

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route exact path="/" component={Index}/>
      <Route exact path="/gallery" component={Gallery}/>
      {/*<Route component={NotFoundPage}/>*/}
    </Switch>
  </Router>
);