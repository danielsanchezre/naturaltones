import React from 'react';

export const NavbarResponsive = () => {

    return (
      <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
          <a className="navbar-brand" href="#" style={{ fontFamily: "Earth Orbiter Extra Bold, serif", fontSize: 28 }}>NATURAL TONES</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item active">
                <a className="nav-link" href="#">INICIO <span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">MUSICA</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">VIDEOS</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">MERCH</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">GALERIA</a>
              </li>
            </ul>
            <div className="form-inline my-2 my-lg-0 ml-2">
              <button className="btn btn-outline-danger my-2 my-sm-0" type="submit" style={{ fontFamily: "Earth Orbiter Bold, serif", borderRadius: 0 }}>CONTACTO</button>
            </div>
          </div>
        </nav>
      </div>
    );
};