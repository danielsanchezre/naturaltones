import React, { Component } from 'react';

class Navbar extends Component {

  state ={classNameNav:"" };

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
  }

  handleScroll = () => {
    if (window.pageYOffset > 100) {
        if(!this.state.classNameNav){

          this.setState({ classNameNav: "bg-nav" });  

        }
    } else {
        if(this.state.classNameNav){

          this.setState({ classNameNav: "" });

        }
    }
  }

  render() {
    return (
      <div>
        <nav className={ "navbar navbar-expand-lg navbar-dark " + this.state.classNameNav + " fixed-top"}>
          <a className="navbar-brand" href="#" style={{ fontFamily: "Earth Orbiter Extra Bold, serif", fontSize: 28 }}>NATURAL TONES</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <a className="nav-link" href="/">INICIO</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://open.spotify.com/artist/4syVdtNU79IVIQ4BAvYJ8p">MUSICA</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://www.youtube.com/channel/UCuIRqj_FkbtAj6sg0RYyWBg">VIDEOS</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="https://merchanfy.com/collections">MERCH</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/gallery">GALERIA</a>
              </li>
            </ul>
            <div className="form-inline my-2 my-lg-0 ml-2">
              <button className="btn btn-outline-danger my-2 my-sm-0" type="submit" style={{ fontFamily: "Earth Orbiter Bold, serif" }}>CONTACTO</button>
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

export default Navbar;