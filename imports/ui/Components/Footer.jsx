import React from 'react';

export const FooterWeb = () => {

    return (
      <div>
        <footer className="page-footer font-small text-light" style={{ backgroundColor: '#111' }}>
          <div className="container">
            <div className="row text-center d-flex justify-content-center pt-5 mb-3">
              <div className="col-md-2 mb-3">
                <h6 className="text-uppercase font-weight-bold">
                  <a href="/">INICIO</a>
                </h6>
              </div>
              <div className="col-md-2 mb-3">
                <h6 className="text-uppercase font-weight-bold">
                  <a href="#!">MUSICA</a>
                </h6>
              </div>
              <div className="col-md-2 mb-3">
                <h6 className="text-uppercase font-weight-bold">
                  <a href="#!">VIDEOS</a>
                </h6>
              </div>
              <div className="col-md-2 mb-3">
                <h6 className="text-uppercase font-weight-bold">
                  <a href="#!">MERCH</a>
                </h6>
              </div>
              <div className="col-md-2 mb-3">
                <h6 className="text-uppercase font-weight-bold">
                  <a href="#!">GALERIA</a>
                </h6>
              </div>
            </div>

            <hr style={{ margin: '0 15%', backgroundColor: 'white' }} />

            <div className="row d-flex text-center justify-content-center mb-md-0 mb-4">
              <div className="col-md-8 col-12 mt-5">
                <p style={{ lineHeight: '1.7rem' }}>
                  <b>Booking:</b> naturaltonescol@gmail.com
                  <br/>
                  <br/>
                  Natural Tones es una banda colombiana que combina subgéneros 
                  del rock (como el rock alternativo, el blues rock, el hard rock y el rock psicodélico) y elementos 
                  de varias culturas globales para crear una atmósfera única en cada una de sus canciones.
                </p>
              </div>
            </div>

            <hr className="clearfix d-md-none" style={{ margin: '10% 15% 5%', backgroundColor: 'white' }} />

            <div className="row">
              <div className="col-md-12 d-flex justify-content-center">
                <div className="mt-3 mb-4 ml-4">
                  <a className="ic" href="https://www.facebook.com/naturaltonescol/">
                    <i className="fab fa-facebook-f fa-lg text-light mr-4"> </i>
                  </a>
                  <a className="ic">
                    <i className="fab fa-twitter fa-lg text-light mr-4"> </i>
                  </a>
                  <a className="ic">
                    <i className="fab fa-instagram fa-lg text-light mr-4"> </i>
                  </a>
                  <a className="ic">
                    <i className="fab fa-spotify fa-lg text-light mr-4"> </i>
                  </a>
                  <a className="ic">
                    <i className="fab fa-deezer fa-lg text-light mr-4"> </i>
                  </a>
                  <a className="ic">
                    <i className="fab fa-youtube fa-lg text-light mr-4"> </i>
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="footer-copyright text-center py-3" style={{ backgroundColor: 'black' }}>© 2020 Copyright:
            <a href="#"> NATURAL TONES</a>
          </div>

          </footer>
      </div>
    );
};