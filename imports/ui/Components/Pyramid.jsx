import React, { Component } from 'react';
import * as THREE from "three";
import { MTLLoader, OBJLoader } from "three-obj-mtl-loader";
import OrbitControls from "three-orbitcontrols";

class Pyramid extends Component {

  componentDidMount() {
    const width = this.mount.clientWidth;
    const height = this.mount.clientHeight;
    const radius = 4;
    const cylinderHeight = 5;
    var ambientLights, lights;
    const piramydGeometry = new THREE.CylinderGeometry(0, radius, cylinderHeight, 4, 1)
    const material = new THREE.MeshStandardMaterial({color: '#555', flatShading: true});
    
    this.scene = new THREE.Scene();
    //Add Renderer
    this.renderer = new THREE.WebGLRenderer({ alpha: true });
    this.renderer.setSize(width, height);
    this.mount.appendChild(this.renderer.domElement);
    //add Camera
    this.camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 2000);
    this.camera.rotation.x = -0.3;
    this.camera.position.y = 0.8;
    this.camera.position.z = 15;
    
    /*var light = new THREE.PointLight(0xffffff, 1, Infinity);
    this.camera.add(light);*/
    //this.camera.position.y = 4;
    
    //Camera Controls
    //const controls = new OrbitControls(this.camera, this.renderer.domElement);
    
    //LIGHTS
    ambientLights = new THREE.HemisphereLight('#aaa', '#333', 3);
    lights = new THREE.PointLight('#555', 10);
    lights.position.set(200,100,50);

    this.cubeMesh = new THREE.Mesh(piramydGeometry, material)

    this.scene.add(lights);
    this.scene.add(ambientLights);
    this.scene.add(this.cubeMesh);
    //ADD Your 3D Models here
    this.createGrid();
    this.renderScene();
    //start animation
    this.start();
  }

  start = () => {
    if (!this.frameId) {
    this.frameId = requestAnimationFrame(this.animate);}
    };
    stop = () => {
    cancelAnimationFrame(this.frameId);
  };

  createGrid = () => {
    var gridHelper = new THREE.GridHelper(20, 20);
    gridHelper.position.y = -2.5;
    gridHelper.position.z = 1.3;
    this.scene.add(gridHelper);
  }

  animate = () => {
    //Animate Models Here
    //ReDraw Scene with Camera and Scene Object
    if (this.cubeMesh) this.cubeMesh.rotation.y += 0.003;

    this.renderScene();
    this.frameId = window.requestAnimationFrame(this.animate);
  };

  renderScene = () => {
    if (this.renderer) this.renderer.render(this.scene, this.camera);
  };

  render() {
    return (
      <div 
      style={{ width: "100%", height: "550px", paddingTop: 60 }}
      ref={mount => { this.mount = mount}}
      />
    )
  }
}

export default Pyramid;