import React, { Component } from 'react';

//Components
import Navbar from '../Components/Navbar.jsx';

import { FooterWeb } from '../Components/Footer.jsx';

class Gallery extends Component {
  render(){
    return (
      <div>
        <Navbar/>
        <div className="jumbotron pt-6"  style ={{ color: "#fff" }}>
          <h1 className="display-4 mt-5" style={{ fontFamily: "Gobold Blocky Regular, serif", color: "#fff", fontSize: 80 }}>GALERÍA</h1>
          <p className="lead">Galería de imagenes en vivo, diseños de la banda y material artístico relacionado a Natural Tones.</p>
          <hr className="my-4" style ={{ backgroundColor: "#fff" }} />
          <p>Si quieres más contenido, puedes seguirnos en nuestras redes sociales a través del siguiente link.</p>
          <a className="btn btn-primary btn-lg" href="#" role="button">Ver más</a>
        </div>
        <section className="galleryPageSectionMedia">
          <div className="container">
            <div className="row">
              <div className="col-lg-4">
                <div class="card">
                  <img src="img/naturaltonesmedia1.jpg" class="card-img-top" alt="..." />
                  <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div class="card">
                  <img src="img/naturaltonesmedia2.jpg" class="card-img-top" alt="..." />
                  <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <div class="card">
                  <img src="img/naturaltonesmedia3.jpg" class="card-img-top" alt="..." />
                  <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <FooterWeb/>
      </div>
    )
  }
}

export default Gallery;