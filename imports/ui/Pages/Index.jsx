import React, { Component } from 'react';

//Components
import Navbar from '../Components/Navbar.jsx';
import { NavbarResponsive } from '../Components/NavbarResponsive.jsx';
import Pyramid from '../Components/Pyramid.jsx';
import { FooterWeb } from '../Components/Footer.jsx';

class Index extends Component {
  render(){
    return (
      <div>
        <section className="mainPageSectionPrime">
          <Navbar/>
          <Pyramid/>
          <h1 className="mainPageText" style={{ fontFamily: "Gobold Blocky Regular, serif" }}>|DISEÑAR UN NUEVO INFINITO|</h1>
        </section>
        <section style={{ padding: '100px 5%' }}>
          <div className="container">
              <h1 className="mb-5" style={{ fontFamily: "Gobold Bold, serif", fontSize: 52 }}>SENCILLOS</h1>
            <div className="row">
              <div className="col-lg-4">
                <p className="text-justify mt-1">
                  De este LP se desprenden los temas estrenados: la canción debut <b>‘Natural’</b>, reestrenada el 16 de 
                  octubre del 2020 bajo el actual nombre de la banda, <b>‘Bajo el Sol’</b>, estrenada el 24 de septiembre 
                  del 2020, y <b>‘Camino’</b>, próxima a ser lanzada en plataformas de streaming.
                </p>
                <button className="btn mt-4 mb-4 btn-suscribe" style={{ fontFamily: "Earth Orbiter Bold, serif" }}>
                  SUSCRIBETE A <i className="fab fa-youtube-square fa-lg"></i>
                </button>
              </div>
              <div className="col-lg-7 offset-lg-1">
                <div className="row">
                  <div className="col-lg-6 mt-2">
                    <center>
                      <a href="https://sl.onerpm.com/3005476086">
                        <img src="img/NaturalPortada.jpg" width="85%" className="imgMedia"/>
                      </a>
                    </center>
                  </div>
                  <div className="col-lg-6 mt-2">
                    <center>
                      <a href="https://sl.onerpm.com/1687263928">
                        <img src="img/BajoelSolPortada.jpg" width="85%" className="imgMedia"/>
                      </a>
                    </center>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
    
        <section className="mainPageSectionBiography">
          <div className="container text-light">
            <div className="row">
              <div className="col-md-4 offset-md-7">
                <h1 className="mb-5 text-center" style={{ fontFamily: "Gobold Blocky Regular, serif", fontSize: 82}}>BIOGRAFIA</h1>
                <p className="text-justify mt-4" style={{ textShadow: '1px 1px 5px #111' }}>
                  Natural Tones es una banda colombiana (originaria de Santa Rosa de Cabal) que combina subgéneros 
                  del rock (como el rock alternativo, el blues rock, el hard rock y el rock psicodélico) y elementos 
                  de varias culturas globales para crear una atmósfera única en cada una de sus canciones.
                  <br/>
                  <br/>
                  Las letras de sus canciones van desde temas como el amor y el desamor, hasta temas metafísicos y 
                  existenciales. Actualmente se encuentran grabando su primer LP titulado ‘En el Bosque’, un álbum 
                  conceptual que narra el viaje de búsqueda individual hacia el encuentro consigo mismo. 
                </p>
              </div>
            </div>
          </div>
        </section>
    
        <section style={{ padding: '100px 5%' }}>
          <div className="container">
            <h1 className="mb-5 text-center" style={{ fontFamily: "Gobold Bold, serif", fontSize: 52}}>EVENTOS</h1>
            <div className="row">
              <div className="col-md-8 offset-md-2">
                <table className="table">
                  <tbody>
                    <tr>
                      <td>
                        <b>17 Oct, 2020</b>
                        <p>Sábado</p>
                      </td>
                      <td>
                        <b>La Chambrana Café - Bar</b>
                        <p>Santa Rosa de Cabal, Colombia</p>
                      </td>
                      <td>
                        <button className="btn btn-outline-success mt-2" style={{ fontFamily: "Earth Orbiter Bold, serif" }}>Tickets</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>22 Ago, 2020</b>
                        <p>Sábado</p>
                      </td>
                      <td>
                        <b>Apertura Juegos Intercolegiados</b>
                        <p>Santa Rosa de Cabal, Colombia</p>
                      </td>
                      <td>
                        <button className="btn btn-outline-success mt-2" style={{ fontFamily: "Earth Orbiter Bold, serif" }}>Tickets</button>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <b>14 Oct, 2019</b>
                        <p>Sábado</p>
                      </td>
                      <td>
                        <b>Shuffle Live Music</b>
                        <p>Pereira, Colombia</p>
                      </td>
                      <td>
                        <button className="btn btn-outline-success mt-2" style={{ fontFamily: "Earth Orbiter Bold, serif" }}>Tickets</button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
    
        <FooterWeb/>
      </div>
    );
  }
};

export default Index;